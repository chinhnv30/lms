/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Object.Librarian;
import Object.Member;
import Object.Person;
import Object.User;
import java.awt.HeadlessException;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author DAT
 */
public class UserControl {

    private static PreparedStatement ps;
    private static ResultSet rs;
    private static Connection conn;
    private static String query;

    public static User login(String user, String password) {
        User urs = null;

        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement("SELECT * FROM user where user = ? and password = ?");
            ps.setString(1, user);
            ps.setString(2, password);
            rs = ps.executeQuery();
            while (rs.next()) {
                urs = new User(rs.getInt("id"),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("role"),
                        new Person(rs.getString("name"), rs.getString("address"), rs.getString("phone_number")));

            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {

            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);

        }

        return urs;
    }

    public static boolean addUser(User user) {
        query = "insert into user (user, name, password, role, phone_number, address) values(?,?,?,?,?,?)";

        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);

            ps.setString(1, user.getUser());
            ps.setString(2, user.person.getName());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getRole());
            ps.setString(5, user.person.getPhoneNumber());
            ps.setString(6, user.person.getAddress());
            ps.execute();
            ps.close();
            JOptionPane.showMessageDialog(null, "Adding successfully");
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Adding failed");
            return false;
        }
        return true;
    }

    public static boolean updateUser(User user) {
        query = "UPDATE user SET id = ? where id = ?";
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);
            ps.setInt(1, user.getId());
            ps.setString(1, user.getUser());
            ps.setString(2, user.person.getName());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getRole());
            ps.setString(5, user.person.getPhoneNumber());
            ps.setString(6, user.person.getAddress());
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            return false;
        }
    }

    public static boolean deleteUser(User user) {
        query = "DELETE FROM user WHERE id = ?";
        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);
            ps.setInt(1, user.getId());
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            return false;
        }
    }

    public static ArrayList<User> getUserList() {
        ArrayList<User> userList = new ArrayList<>();
        try {

            Statement st = ConnectDB.getInstance().getConnect().createStatement();
            rs = st.executeQuery("select * from user");

            while (rs.next()) {
                User user = new User(Integer.parseInt(rs.getString("id")),
                        rs.getString("user"),
                        rs.getString("password"),
                        rs.getString("role"),
                        new Person(rs.getString("name"),
                                rs.getString("address"),
                                rs.getString("phone_number")
                        )
                );
                userList.add(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userList;
    }

    public static void showUserList(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        ArrayList<User> userlist = getUserList();
        Object[] row = new Object[5];

        for (int i = 0; i < userlist.size(); i++) {

            row[0] = userlist.get(i).person.getName();
            row[1] = userlist.get(i).getUser();
            row[2] = userlist.get(i).getRole();
            row[3] = userlist.get(i).person.getPhoneNumber();
            row[4] = userlist.get(i).person.getAddress();
            model.addRow(row);
        }

    }

    public static User getUserById(int id) {
        User user = null;
        query = "delete from user where id = ?";
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            ps.setInt(1, id);
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }
}
