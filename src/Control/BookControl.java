/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Object.Author;
import Object.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author DAT
 */
public class BookControl {

    private static PreparedStatement ps;
    private static ResultSet rs;
    private static String query;
    private static Connection conn;

    public static boolean addBook(Book book, Author author) {
        query = "call addBook(?,?,?,?,?,?)";    //(isbn title subject publisher number_of_page author_name)
        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);
            ps.setString(1, book.getIsbn());
            ps.setString(2, book.getTitle());
            ps.setString(3, book.getSubject());
            ps.setString(4, book.getPublisher());
            ps.setString(5, book.getNumberOfPage());
            ps.setString(6, author.getName());
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(BookControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public static boolean updateBook(Book book) {
        query = "call updateBook(?,?,?,?,?)";   //(title subject publisher, number_of_page)

        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);
            ps.setString(1, book.getIsbn());
            ps.setString(2, book.getTitle());
            ps.setString(3, book.getIsbn());
            ps.setString(4, book.getIsbn());
            ps.setString(5, book.getIsbn());
            ps.close();
        } catch (Exception e) {
            Logger.getLogger(BookControl.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
        
        return false;
    }
    public static void filterBook(JTable table, String text) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        String query;
        if (text.trim() == null || text.trim() == "") {
            query = "SELECT * FROM book";
        } else {
            query = "SELECT * FROM book where title like '%" + text.trim() + "%' OR isbn like '%" + text.trim() + "%'";
        }
//        ps = ConnectDB.getInstance().getConnect().prepareStatement("SELECT * FROM user where user = ? and password = ?");
//        User urs = null;
        System.out.println("dwfegrehtjrydtk " + query);
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("dwfegrehtjrydtk " + rs.getString("isbn"));
                Object[] row = new Object[5];
                row[0] = rs.getString("isbn");
                row[1] = rs.getString("title");
                row[2] = rs.getString("subject");
                row[3] = rs.getString("publisher");
                row[4] = rs.getString("number_of_page");
                model.addRow(row);
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

}
