/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import Control.UserControl;
import Object.Person;
import javax.swing.JOptionPane;

/**
 *
 * @author DAT
 */
public class Librarian extends User {

    public Librarian() {
    }

    public Librarian(int id, Person person, String account, String password, String role) {
        super(id, account, password, role, person);
    }

    
   public void addUser(User user) {
        if (UserControl.addUser(user)) {
            JOptionPane.showMessageDialog(null, "Adding completeted");
        } else {
            JOptionPane.showMessageDialog(null, "Adding failed");
        }
    }
    
    public void deleteUser(User user) {
        if (UserControl.deleteUser(user)) {
            JOptionPane.showMessageDialog(null, "Deleting completeted");
        } else {
            JOptionPane.showMessageDialog(null, "Deleting failed");
        }
    }
}
