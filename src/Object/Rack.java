/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

/**
 *
 * @author DAT
 */
public class Rack {
    private int id;
    private String location_id;

    public Rack() {
    }

    public Rack(int id, String location_id) {
        this.id = id;
        this.location_id = location_id;
    }

    public int getId() {
        return id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }
    
    
}
