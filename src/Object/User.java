/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

/**
 *
 * @author DAT
 */
public class User {

    private int id;
    private String user;
    private String password;
    private String role;
    public Person person;

    public User() {
    }

    public User(int id, String user, String password, String role, Person person) {
        this.id = id;
        this.user = user;
        this.password = password;
        this.role = role;
        this.person = person;
    }

    public int getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
