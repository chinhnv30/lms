/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import java.sql.Date;

/**
 *
 * @author DAT
 */
public class BookItem extends Book {
    private int id;
    private String barcode;
    private String status;
    private double price;
    private Date publicationDate;

    public BookItem() {
    }

    public BookItem(int id, String barcode, String status, double price, Date publicationDate, String isbn, String title, String subject, String publisher, String numberOfPage) {
        super(isbn, title, subject, publisher, numberOfPage);
        this.id = id;
        this.barcode = barcode;
        this.status = status;
        this.price = price;
        this.publicationDate = publicationDate;
    }

    public BookItem(String barcode, String status, double price, Date publicationDate, String isbn, String title, String subject, String publisher, String numberOfPage) {
        super(isbn, title, subject, publisher, numberOfPage);
        this.barcode = barcode;
        this.status = status;
        this.price = price;
        this.publicationDate = publicationDate;
    }

    public int getId() {
        return id;
    }


    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    
    public void checkout(){};
    
    
}
