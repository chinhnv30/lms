/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Process;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author DAT
 */
public class Utils {
     public static void exitOptionDialog(Frame frame) {
        String[] options = {"No", "Yes"};
        int x = JOptionPane.showOptionDialog(null, "Are you want to exit?", "Quit", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (x == 1) {
            System.exit(0);
        }
//    int answer = JOptionPane.showConfirmDialog(this, "Are you want to exit?", "Quit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, JOptionPane);
//    if (answer == JOptionPane.YES_OPTION){
//        System.exit(0);
//    }
    }
     public static void preventClosingWindowFromCrossButton(Frame frame)
     {
         frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                Utils.exitOptionDialog(frame);
            }
        });
     }
     
}
